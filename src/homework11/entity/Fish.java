package homework11.entity;

import java.util.Set;

public class Fish extends Pet {
    private Species species = Species.FISH;

    public Fish() {
        super();
    }
    public Fish(String nickname) {
        super(nickname);
    }

    public Fish(String nickname, int age, int trickLevel, Set<String> habits){
        super(nickname, age, trickLevel, habits);
    }

    public Species getSpecies(){ return this.species; }

    @Override
    void respond(){
        System.out.printf("Привет, хозяин. Я - %s. Я соскучился!\n", getNickname());
    }
}
