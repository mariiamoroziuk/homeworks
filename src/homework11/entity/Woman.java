package homework11.entity;

import java.util.Map;

public class Woman extends Human {
    Woman() {
        super();
    }
    public Woman(String name, String surname, int year) {
        super(name, surname, year);
    }

    public Woman(String name, String surname, int year, int iq, Map<DayOfWeek, String> schedule){
        super(name, surname, year, iq, schedule);
    }

    @Override
    void greetPet(){
        if(this.pet != null)System.out.printf("Привет, золотце %s\n", this.pet.getNickname());
        else System.out.print("So sad that I don't have a pet :(\n");
    }

    void makeup(){
        System.out.println("Я крашу губы ;)\n");
    }
}
