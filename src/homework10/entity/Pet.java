package homework10.entity;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

public abstract class Pet {
        private Species species = Species.UNKNOWN;
        private String nickname;
        private int age;
        private int trickLevel;
        private Set<String> habits = new HashSet<>();

        static {
            System.out.println("Загружается новый класс Pet");
        }
        {
            System.out.println("Создается новый обьект класса Pet");
        }

        Pet(){
        }
        Pet(String nickname){
            setNickname(nickname);
        }
        Pet(String nickname, int age, int trickLevel, Set<String> habits){
            this(nickname);
            setAge(age);
            setTrickLevel(trickLevel);
            setHabits(habits);
        }

        public Species getSpecies(){ return this.species; }
        public void setSpecies(Species species) { this.species=species; }

        public String getNickname(){
            return this.nickname;
        }
        public void setNickname(String nickname) {
            this.nickname=nickname;
        }

        public int getAge(){
            return this.age;
        }
        public void setAge(int age) {
            this.age=age;
        }

        public int getTrickLevel(){
            return this.trickLevel;
        }
        public void setTrickLevel(int trickLevel) {
            if (trickLevel > 100||trickLevel < 0) {
                System.out.println("Wrong trickLevel");
                return;
            }
            this.trickLevel=trickLevel;
        }

        public Set<String> getHabits(){
            return this.habits;
        }
        public void setHabits(Set<String> habits) {
            this.habits=habits;
        }

        void eat(){
            System.out.println("Я кушаю!");
        }

        abstract void respond();

        @Override
        public String toString(){
            return this.getSpecies()+"{nickname="+this.getNickname()+", age="+this.getAge()+", trickLevel="+this.getTrickLevel()+", habits="+this.getHabits()+", canFly="+(this.getSpecies() != null ? this.getSpecies().getCanFly() : null)
                    +", numberOfLegs="+(this.getSpecies() != null ? this.getSpecies().getNumberOfLegs() : null)+ ", hasFur="+(this.getSpecies() != null ? this.getSpecies().getHasFur() : null)+"}";
        }

        @Override
        public boolean equals(Object obj){
            if (this == obj) return true;
            if (!(obj instanceof Pet)) return false;

            Pet pet = (Pet)obj;
            return (Objects.equals(this.getSpecies(), pet.getSpecies()) && Objects.equals(this.getNickname(), pet.getNickname()));
        }

        @Override
        public int hashCode() {
            return Objects.hash(this.getSpecies(), this.getNickname());
        }

        @Override
        protected void finalize() throws Throwable {
            System.out.printf("Deleted object: %s", this.toString());
        }

    }
