package homework8;

public interface IHumanCreator {
    Human bornChild(String boysName, String girlsName);
}
