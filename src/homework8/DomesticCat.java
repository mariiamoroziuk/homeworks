package homework8;

import java.util.Set;

public class DomesticCat extends Pet implements IFoul {
    private Species species = Species.DOMESTIC_CAT;

    DomesticCat() {
        super();
    }
    DomesticCat(String nickname) {
        super(nickname);
    }

    DomesticCat(String nickname, int age, int trickLevel, Set<String> habits){
        super(nickname, age, trickLevel, habits);
    }

    public Species getSpecies(){ return this.species; }

    @Override
    void respond() {
        System.out.printf("Привет, хозяин. Я - %s. Я соскучился!\n", getNickname());
    }

    @Override
    public void foul() {
        System.out.println("Нужно хорошо замести следы...");
    }

}
