package homework13;

import homework13.console.ConsoleMenu;

import java.io.IOException;

public class HappyFamily {
    public static void main(String[] args) throws IOException {
        HappyFamilyContext.init();
        ConsoleMenu.start();
    }
}
