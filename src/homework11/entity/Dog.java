package homework11.entity;

import java.util.Set;

public class Dog extends Pet implements IFoul {

    private Species species = Species.DOG;

    Dog() {
        super();
    }
    Dog(String nickname) {
        super(nickname);
    }

    public Dog(String nickname, int age, int trickLevel, Set<String> habits){
        super(nickname, age, trickLevel, habits);
    }

    public Species getSpecies(){ return this.species; }

    @Override
    public void respond(){
        System.out.printf("Привет, хозяин. Я - %s. Я соскучился!\n", this.getNickname());
    }

    @Override
    public void foul() {
        System.out.println("Нужно хорошо замести следы...");
    }
}
