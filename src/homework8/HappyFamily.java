package homework8;

import java.util.HashSet;
import java.util.Random;
import java.util.Set;

public class HappyFamily {
    public static void main(String[] args) {
        Set<String> petHabits = new HashSet<>();
        petHabits.add("кушать");
        petHabits.add("спать");
        petHabits.add("гулять");

        Dog petty=new Dog("колобок", 2, 80, petHabits);

        System.out.println(petty);
        petty.respond();

        Man man = new Man("Алексей", "Потапенко", 1980);
        man.setIq(80);
        Woman woman = new Woman("Настя", "Каменских", 1990);
        woman.setIq(70);

        Family family = new Family(woman,man);

        Random random = new Random();
        int randomNumName = random.nextInt(10);

        family.bornChild(Names.values()[randomNumName].name(), Names.values()[randomNumName+10].name());
        System.out.print(family);
    }
}
