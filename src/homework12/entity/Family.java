package homework12.entity;

import java.util.*;

public class Family implements IHumanCreator {
    private Human mother;
    private Human father;
    private List<Human> children = new ArrayList<>();
    private Set<Pet> pets = new HashSet<>();
    protected long id;

    static {
        System.out.println("Загружается новый класс Family");
    }
    {
        System.out.println("Создается новый обьект класса Family");
    }

    public Family(Human mother, Human father){
        setMother(mother);
        setFather(father);
        mother.setFamily(this);
        father.setFamily(this);
    }
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Human getMother(){ return this.mother; }
    public void setMother(Human mother) { this.mother=mother; }

    public Human getFather(){ return this.father; }
    public void setFather(Human father) { this.father=father; }

    public List<Human> getChildren(){
        return this.children;
    }
    public void setChildren(List<Human> children) { this.children=children; }

    public Set<Pet> getPets(){
        return this.pets;
    }
    public void setPets(Set<Pet> pets) {
        this.pets=pets;
    }

    public void addChild(Human child){
        this.children.add(child);
        child.setFamily(this);
    }

    public boolean deleteChild(int index){
        if(index < 0 || index > this.children.size()-1){
            return false;
        }
        return deleteChild(this.children.get(index));
    }

    public boolean deleteChild(Human child){
        child.setFamily(null);
        return this.children.remove(child);
    }

    public void addPet(Pet pet){
        this.pets.add(pet);
    }

    boolean deletePet(Pet pet){
        return this.pets.remove(pet);
    }

    public int countFamily(){
        return this.children.size() + 2;
    }

    public String prettyFormat(){
        return "family:\n"
                + "       mother: " + this.mother.prettyFormat()
                + "       father: " + this.father.prettyFormat()
                + "       children: " + this.getChildren().stream().map(Human::prettyFormat).reduce("", (x, y)->x+y)
                + "       pets: " + this.getPets().stream().map(Pet::prettyFormat).reduce("", (x, y)->x+y);
    }

    @Override
    public String toString(){
        return "Mother:"+this.getMother().toString() + "Father:"+this.getFather().toString() + "Children:"+this.getChildren() +"Pets:"+this.getPets();
    }

    @Override
    public boolean equals(Object obj){
        if (this == obj) return true;
        if (!(obj instanceof Family)) return false;

        Family family = (Family)obj;
        return (Objects.equals(this.getMother(), family.getMother()) && Objects.equals(this.getFather(), family.getFather()));
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.getMother(), this.getFather());
    }

    @Override
    protected void finalize() throws Throwable {
        System.out.printf("Deleted object: %s", this.toString());
    }

    @Override
    public Human bornChild(String boysName, String girlsName) {
        Random random = new Random();
        int randomNumGender = random.nextInt(2);

        String childName = randomNumGender == 0 ? boysName : girlsName;
        int childIq = (this.mother.getIq() + this.father.getIq()) / 2;
        long birthDay = new Date().getTime();

        Map<DayOfWeek, String> childSchedule= new HashMap<>();
//        childSchedule.put(DayOfWeek.MONDAY, "спать, кушать");
//        childSchedule.put(DayOfWeek.TUESDAY, "спать, кушать");
//        childSchedule.put(DayOfWeek.WEDNESDAY, "спать, кушать");
//        childSchedule.put(DayOfWeek.THURSDAY, "спать, кушать");
//        childSchedule.put(DayOfWeek.FRIDAY, "спать, кушать");
//        childSchedule.put(DayOfWeek.SATURDAY, "спать, кушать");
//        childSchedule.put(DayOfWeek.SUNDAY, "спать, кушать");

        Human child;
        if(randomNumGender == 0){
            child = new Man(childName, this.father.getSurname(), birthDay, childIq, childSchedule);
        } else {
            child = new Woman(childName, this.father.getSurname(), birthDay, childIq, childSchedule);
        }
        this.addChild(child);
        child.setFamily(this);
        return child;
    }
}
