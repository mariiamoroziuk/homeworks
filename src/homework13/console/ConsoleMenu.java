package homework13.console;

import homework13.entity.*;
import homework13.HappyFamilyContext;
import homework13.logger.FamilyLogger;

import java.io.IOException;
import java.util.Scanner;

public class ConsoleMenu {
    public static void start() throws IOException {
        Scanner scanner = new Scanner(System.in);
        boolean run = true;

        while (run) {
            printMenuItems();
            String input = scanner.nextLine();

            switch (input) {
                case "1":
                    HappyFamilyContext.familyController.loadDataFromDB();
                    break;
                case "2":
                    HappyFamilyContext.familyController.displayAllFamilies();
                    break;
                case "3":
                    System.out.println(HappyFamilyContext.familyController.getFamiliesBiggerThan(readNumber(scanner)));
                    break;
                case "4":
                    System.out.println(HappyFamilyContext.familyController.getFamiliesLessThan(readNumber(scanner)));
                    break;
                case "5":
                    System.out.println(HappyFamilyContext.familyController.countFamiliesWithMemberNumber(readNumber(scanner)));
                    break;
                case "6":
                    System.out.println("Введите имя матери:");
                    String motherName = scanner.nextLine();
                    System.out.println("Введите фамилию матери:");
                    String motherSurname = scanner.nextLine();
                    System.out.println("Введите год рождения матери:");
                    String motherBirthYear = scanner.nextLine();
                    System.out.println("Введите месяц рождения матери:");
                    String motherBirthMonth = scanner.nextLine();
                    System.out.println("Введите день рождения матери:");
                    String motherBirthDay = scanner.nextLine();
                    System.out.println("Ig матери");
                    int motherIq = readNumber(scanner);

                    System.out.println("Введите имя отца:");
                    String fatherName = scanner.nextLine();
                    System.out.println("Введите фамилию отца:");
                    String fatherSurname = scanner.nextLine();
                    System.out.println("Введите год рождения отца:");
                    String fatherBirthYear = scanner.nextLine();
                    System.out.println("Введите месяц рождения отца:");
                    String fatherBirthMonth = scanner.nextLine();
                    System.out.println("Введите день рождения отца:");
                    String fatherBirthDay = scanner.nextLine();
                    System.out.println("Ig отца");
                    int fatherIq = readNumber(scanner);

                    HappyFamilyContext.familyController.createNewFamily(new Human(motherName, motherSurname, motherBirthDay + "/" + motherBirthMonth + "/" + motherBirthYear, motherIq), new Human(fatherName, fatherSurname, fatherBirthDay + "/" + fatherBirthMonth + "/" + fatherBirthYear, fatherIq));
                    break;
                case "7":
                    System.out.println("Id семьи");
                    HappyFamilyContext.familyController.deleteFamilyByIndex(readNumber(scanner));
                    break;
                case "8":
                    boolean subRun = true;
                    while (subRun){
                        printSubMenuItems();
                        String subMenuItem = scanner.nextLine();
                    switch (subMenuItem) {
                        case "1":
                            System.out.println("Id семьи");
                            int subFamilyNumber = readNumber(scanner);
                            System.out.println("Введите имя для девочки:");
                            String subFamilyGirlName = scanner.nextLine();
                            System.out.println("Введите имя для мальчика:");
                            String subFamilyBoyName = scanner.nextLine();
                            HappyFamilyContext.familyController.bornChild(HappyFamilyContext.familyController.getFamilyByIndex(2), "test", "test");
                            HappyFamilyContext.familyController.bornChild(HappyFamilyContext.familyController.getFamilyByIndex(subFamilyNumber), subFamilyBoyName, subFamilyGirlName);
                            break;
                        case "2":
                            System.out.println("Id семьи");
                            int subFamilyNumber2 = readNumber(scanner);
                            System.out.println("Введите имя ребенка:");
                            String childName = scanner.nextLine();
                            System.out.println("Введите фамилию ребенка:");
                            String childSurname = scanner.nextLine();
                            System.out.println("Введите год рождения ребенка:");
                            String childBirthYear = scanner.nextLine();
                            System.out.println("Введите месяц рождения ребенка:");
                            String childBirthMonth = scanner.nextLine();
                            System.out.println("Введите день рождения ребенка:");
                            String childBirthDay = scanner.nextLine();
                            System.out.println("Ig ребенка");
                            int childIq = readNumber(scanner);
                            HappyFamilyContext.familyController.adoptChild(HappyFamilyContext.familyController.getFamilyByIndex(subFamilyNumber2), new Human(childName, childSurname, childBirthDay + "/" + childBirthMonth + "/" + childBirthYear, childIq));
                            break;
                        case "3":
                            subRun = false;
                            break;
                        default:
                            System.out.println("Unknown command");
                            break;
                        }
                    }
                    break;
                case "9":
                    HappyFamilyContext.familyController.deleteAllChildrenOlderThen(readNumber(scanner));
                    break;
                case "10":
                    HappyFamilyContext.familyController.loadDataToDB(HappyFamilyContext.familyController.getAllFamilies());
                    break;
                case "exit":
                    run = false;
                    break;
                default:
                    System.out.println("Unknown command");
                    break;
            }
        }
    }

    public static void printMenuItems(){
        System.out.println("Please enter command");
        System.out.println("1. Загрузить семьи из базы данных");
        System.out.println("2. Отобразить весь список семей");
        System.out.println("3. Отобразить список семей, где количество людей больше заданного");
        System.out.println("4. Отобразить список семей, где количество людей меньше заданного");
        System.out.println("5. Подсчитать количество семей, где количество членов равно заданному");
        System.out.println("6. Создать новую семью");
        System.out.println("7. Удалить семью по индексу семьи в общем списке");
        System.out.println("8. Редактировать семью по индексу семьи в общем списке ");
        System.out.println("9. Удалить всех детей старше возраста");
        System.out.println("10. Сохранить все семьи в базе данных");
    }
    private static void printSubMenuItems(){
        System.out.println("Please enter command");
        System.out.println("1. Родить ребенка");
        System.out.println("2. Усыновить ребенка");
        System.out.println("3. Вернуться в главное меню");
    }

    private static int readNumber(Scanner scanner) {
        int res = 0;
        while (res == 0){
            System.out.println("Введите число:");
            String num = scanner.nextLine();
            try {
                res = Integer.parseInt(num);
            } catch (Exception e) {
                FamilyLogger.error("считывание введенного пользователем числа");
                System.out.println("Это не число.");
            }
        }
        return res;
    }
}



