package homework6;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PetTest {
    Pet pet = new Pet();
    Pet pet2 = new Pet(Species.CHICKEN, "coco");
    String[] habits={"кушать", "спать", "гулять"};
    Pet pet3 = new Pet(Species.CHICKEN, "someone", 2, 5, habits);
    Pet pet4 = pet2;
    Pet pet5 = new Pet();
    Human human = new Human("aa","bb", 1);

    @Test
    void testToString() {
        String result = "null{nickname=null, age=0, trickLevel=0, habits=null, canFly=null, numberOfLegs=null, hasFur=null}";
        assertEquals(result, pet.toString());

        String result2 = "CHICKEN{nickname=coco, age=0, trickLevel=0, habits=null, canFly=false, numberOfLegs=2, hasFur=false}";
        assertEquals(result2, pet2.toString());

        String result3 = "CHICKEN{nickname=someone, age=2, trickLevel=5, habits=[кушать, спать, гулять], canFly=false, numberOfLegs=2, hasFur=false}";
        assertEquals(result3, pet3.toString());
    }

    @Test
    void testEquals() {
        assertTrue(pet2.equals(pet4));
        assertTrue(!pet.equals(pet3));
        assertTrue(!pet3.equals(pet4));
        assertTrue(pet.equals(pet5));
        assertTrue(!human.equals(pet5));
        assertTrue(!pet.equals(human));
    }

    @Test
    void testHashCode() {
        assertTrue(pet2.hashCode() == pet4.hashCode());
        assertTrue(pet.hashCode() != pet3.hashCode());
        assertTrue(pet3.hashCode() != pet4.hashCode());
        assertTrue(pet.hashCode() == pet5.hashCode());
        assertTrue(pet2.hashCode() != human.hashCode());
    }
}