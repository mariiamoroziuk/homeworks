package homework10.dao;

import homework10.entity.*;

import java.util.List;

public interface IFamilyDao {
    List<Family> getAllFamilies();

    Family getFamilyByIndex (int index);

    Family getFamilyById (long id);

    boolean deleteFamily (int index);

    boolean deleteFamily (Family family);

    void saveFamily (Family family);
}
