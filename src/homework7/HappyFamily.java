package homework7;

import homework6.Pet;
import homework6.Species;

public class HappyFamily {
    public static void main(String[] args) {
        String[] petHabits={"кушать", "спать", "гулять"};
        Dog petty=new Dog("колобок", 2, 80, petHabits);
        System.out.println(petty);
        petty.respond();


        Man man = new Man("Алексей", "Потапенко", 1980);
        man.setIq(80);
        Man woman = new Man("Настя", "Каменских", 1990);
        woman.setIq(70);

        Family family = new Family(woman,man);
        family.bornChild();
        System.out.println(family.getChildren()[0].toString());
    }
}
