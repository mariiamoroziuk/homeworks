package homework10.controller;

import homework10.entity.*;
import homework10.service.FamilyService;

import java.util.List;
import java.util.Set;

public class FamilyController {

    private final FamilyService familyService = new FamilyService();

    public List<Family> getAllFamilies(){
        return this.familyService.getAllFamilies();
    }

    public void displayAllFamilies() {
        this.familyService.displayAllFamilies();
    }

    public List<Family> getFamiliesBiggerThan(int num){
        return this.familyService.getFamiliesBiggerThan(num);
    }

    public List<Family> getFamiliesLessThan(int num){
        return this.familyService.getFamiliesLessThan(num);
    }

    public int countFamiliesWithMemberNumber(int num){
        return this.familyService.countFamiliesWithMemberNumber(num);
    }

    public void createNewFamily(Human mother, Human father){
        this.familyService.createNewFamily(mother, father);
    }

    public boolean deleteFamilyByIndex(int index){
        return this.familyService.deleteFamilyByIndex(index);
    }

    public Family bornChild(Family family, String boysName, String girlsName){
        return this.familyService.bornChild(family, boysName, girlsName);
    }

    public Family adoptChild(Family family, Human child) {
        return this.familyService.adoptChild(family, child);
    }

    public void deleteAllChildrenOlderThen(int age) {
        this.familyService.deleteAllChildrenOlderThen(age);
    }

    public int count(){
        return this.familyService.count();
    }

    public Family getFamilyById(long id){
        return this.familyService.getFamilyById(id);
    }

    public Set<Pet> getPets(int index){
        return familyService.getPets(index);
    }

    public void addPet(int index, Pet pet){
        this.familyService.addPet(index, pet);
    }
}
