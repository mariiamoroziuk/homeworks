package homework11.service;

import homework11.dao.*;
import homework11.entity.*;
import java.time.LocalDate;
import java.time.Period;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class FamilyService {
    private IFamilyDao familyDao = new CollectionFamilyDao();

    public List<Family> getAllFamilies(){
        return this.familyDao.getAllFamilies();
    }

    public void displayAllFamilies(){
        this.familyDao.getAllFamilies().forEach(family -> System.out.println(family.toString()));
    }

    public List<Family> getFamiliesBiggerThan(int num){
        return this.familyDao.getAllFamilies()
                .stream()
                .filter(family -> family.countFamily() > num)
                .peek(f-> System.out.println(f.toString()))
                .collect(Collectors.toList());
    }

    public List<Family> getFamiliesLessThan(int num){
        return this.familyDao.getAllFamilies()
                .stream()
                .filter(family -> family.countFamily() < num)
                .peek(f-> System.out.println(f.toString()))
                .collect(Collectors.toList());
    }

    public int countFamiliesWithMemberNumber(int num){
        return (int)this.familyDao.getAllFamilies()
                .stream()
                .filter(family -> family.countFamily() == num)
                .count();
    }

    public void createNewFamily(Human mother, Human father){
        Family family = new Family(mother, father);
        this.familyDao.saveFamily(family);
    }

    public boolean deleteFamilyByIndex(int index){
        return this.familyDao.deleteFamily(index);
    }

    public Family bornChild(Family family, String boysName, String girlsName){
        family.bornChild(boysName, girlsName);
        this.familyDao.saveFamily(family);
        return family;
    }

    public Family adoptChild(Family family, Human child) {
        family.addChild(child);
        this.familyDao.saveFamily(family);
        return family;
    }

    public void deleteAllChildrenOlderThen(int age) {
        LocalDate today = LocalDate.now();

        this.familyDao.getAllFamilies()
                .forEach(family -> family.setChildren(
                        family.getChildren()
                                .stream()
                                .filter(child -> {
                                    LocalDate birthday = LocalDate.ofEpochDay(child.getBirthDate()/1000/60/60/24);
                                    Period p = Period.between(birthday, today);
                                    return p.getYears() <= age;
                                })
                                .collect(Collectors.toList())));
    }

    public int count(){
        return this.familyDao.getAllFamilies().size();
    }

    public Family getFamilyById(long id){
        return this.familyDao.getFamilyById(id);
    }

    public Set<Pet> getPets(int index){
        return this.familyDao.getFamilyByIndex(index).getPets();
    }

    public void addPet(int index, Pet pet){
        Family family = this.familyDao.getFamilyByIndex(index);
        family.addPet(pet);
        this.familyDao.saveFamily(family);
    }

}
