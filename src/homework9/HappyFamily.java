package homework9;

import homework8.*;
import homework9.controller.FamilyController;
import homework9.dao.CollectionFamilyDao;
import homework9.service.FamilyService;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static java.util.stream.Collectors.groupingBy;

public class HappyFamily {
    public static void main(String[] args) {
        Set<String> petHabits = new HashSet<>();
        petHabits.add("кушать");
        petHabits.add("спать");
        petHabits.add("гулять");

        Dog petty=new Dog("колобок", 2, 80, petHabits);


        Man man = new Man("Алексей", "Потапенко", 1980);
        man.setIq(80);
        Woman woman = new Woman("Настя", "Каменских", 1990);
        woman.setIq(70);

        Man man2 = new Man("Костик", "Ильченко", 1980);
        man.setIq(85);
        Woman woman2 = new Woman("Вика", "Русак", 1990);
        woman.setIq(92);
        Man man3 = new Man("Ваня", "Иванов", 2018);
        man.setIq(35);

        FamilyController familyController = new FamilyController();

        familyController.createNewFamily(woman,man);
        familyController.createNewFamily(woman2,man2);

        System.out.println(familyController.getAllFamilies());

        familyController.bornChild(familyController.getFamilyById(1), "Игорь", "Ирина");

        familyController.adoptChild(familyController.getFamilyById(2), man3);

        familyController.addPet(0, petty);

        System.out.println(familyController.getPets(0));

        familyController.displayAllFamilies();

        System.out.println(familyController.getFamiliesBiggerThan(2));

        System.out.println(familyController.getFamiliesLessThan(5));

        System.out.println(familyController.countFamiliesWithMemberNumber(2));

        System.out.println(familyController.deleteFamilyByIndex(1));

        familyController.deleteAllChildrenOlderThen(18);

        System.out.println(familyController.count());

    }
}
