package homework12.console;

import homework12.controller.FamilyController;
import homework12.entity.Human;

import java.util.Scanner;

public class ConsoleMenu {
    public static void start() {
        Scanner scanner = new Scanner(System.in);
        boolean run = true;
        FamilyController familyController = new FamilyController();

        while (run) {
            printMenuItems();
            String input = scanner.nextLine();

            switch (input) {
                case "1":
                    createBD(familyController);
                    break;
                case "2":
                    familyController.displayAllFamilies();
                    break;
                case "3":
                    System.out.println(familyController.getFamiliesBiggerThan(readNumber(scanner)));
                    break;
                case "4":
                    System.out.println(familyController.getFamiliesLessThan(readNumber(scanner)));
                    break;
                case "5":
                    System.out.println(familyController.countFamiliesWithMemberNumber(readNumber(scanner)));
                    break;
                case "6":
                    System.out.println("Введите имя матери:");
                    String motherName = scanner.nextLine();
                    System.out.println("Введите фамилию матери:");
                    String motherSurname = scanner.nextLine();
                    System.out.println("Введите год рождения матери:");
                    String motherBirthYear = scanner.nextLine();
                    System.out.println("Введите месяц рождения матери:");
                    String motherBirthMonth = scanner.nextLine();
                    System.out.println("Введите день рождения матери:");
                    String motherBirthDay = scanner.nextLine();
                    System.out.println("Ig матери");
                    int motherIq = readNumber(scanner);

                    System.out.println("Введите имя отца:");
                    String fatherName = scanner.nextLine();
                    System.out.println("Введите фамилию отца:");
                    String fatherSurname = scanner.nextLine();
                    System.out.println("Введите год рождения отца:");
                    String fatherBirthYear = scanner.nextLine();
                    System.out.println("Введите месяц рождения отца:");
                    String fatherBirthMonth = scanner.nextLine();
                    System.out.println("Введите день рождения отца:");
                    String fatherBirthDay = scanner.nextLine();
                    System.out.println("Ig отца");
                    int fatherIq = readNumber(scanner);

                    familyController.createNewFamily(new Human(motherName, motherSurname, motherBirthDay + "/" + motherBirthMonth + "/" + motherBirthYear, motherIq), new Human(fatherName, fatherSurname, fatherBirthDay + "/" + fatherBirthMonth + "/" + fatherBirthYear, fatherIq));
                    break;
                case "7":
                    System.out.println("Id семьи");
                    familyController.deleteFamilyByIndex(readNumber(scanner));
                    break;
                case "8":
                    boolean subRun = true;
                    while (subRun){
                        printSubMenuItems();
                        String subMenuItem = scanner.nextLine();
                    switch (subMenuItem) {
                        case "1":
                            System.out.println("Id семьи");
                            int subFamilyNumber = readNumber(scanner);
                            System.out.println("Введите имя для девочки:");
                            String subFamilyGirlName = scanner.nextLine();
                            System.out.println("Введите имя для мальчика:");
                            String subFamilyBoyName = scanner.nextLine();
                            familyController.bornChild(familyController.getFamilyByIndex(2), "test", "test");
                            familyController.bornChild(familyController.getFamilyByIndex(subFamilyNumber), subFamilyBoyName, subFamilyGirlName);
                            break;
                        case "2":
                            System.out.println("Id семьи");
                            int subFamilyNumber2 = readNumber(scanner);
                            System.out.println("Введите имя ребенка:");
                            String childName = scanner.nextLine();
                            System.out.println("Введите фамилию ребенка:");
                            String childSurname = scanner.nextLine();
                            System.out.println("Введите год рождения ребенка:");
                            String childBirthYear = scanner.nextLine();
                            System.out.println("Введите месяц рождения ребенка:");
                            String childBirthMonth = scanner.nextLine();
                            System.out.println("Введите день рождения ребенка:");
                            String childBirthDay = scanner.nextLine();
                            System.out.println("Ig ребенка");
                            int childIq = readNumber(scanner);
                            familyController.adoptChild(familyController.getFamilyByIndex(subFamilyNumber2), new Human(childName, childSurname, childBirthDay + "/" + childBirthMonth + "/" + childBirthYear, childIq));
                            break;
                        case "3":
                            subRun = false;
                            break;
                        default:
                            System.out.println("Unknown command");
                            break;
                        }
                    }
                    break;
                case "9":
                    familyController.deleteAllChildrenOlderThen(readNumber(scanner));
                    break;
                case "exit":
                    run = false;
                    break;
                default:
                    System.out.println("Unknown command");
                    break;
            }
        }
    }

    public static void printMenuItems(){
        System.out.println("Please enter command");
        System.out.println("1. Заполнить тестовыми данными");
        System.out.println("2. Отобразить весь список семей");
        System.out.println("3. Отобразить список семей, где количество людей больше заданного");
        System.out.println("4. Отобразить список семей, где количество людей меньше заданного");
        System.out.println("5. Подсчитать количество семей, где количество членов равно заданному");
        System.out.println("6. Создать новую семью");
        System.out.println("7. Удалить семью по индексу семьи в общем списке");
        System.out.println("8. Редактировать семью по индексу семьи в общем списке ");
        System.out.println("9. Удалить всех детей старше возраста");
    }
    private static void printSubMenuItems(){
        System.out.println("Please enter command");
        System.out.println("1. Родить ребенка");
        System.out.println("2. Усыновить ребенка");
        System.out.println("3. Вернуться в главное меню");
    }
    private static void createBD(FamilyController familyController){
        familyController.createNewFamily(new Human("Елена", "Сом", "12/04/1999", 75),
                new Human("Дмитрий", "Страшенко", "19/08/1997", 80));
        familyController.createNewFamily(new Human("Ирина", "Соловий", "01/11/2001", 95),
                new Human("Олег", "Донец", "11/09/1998", 80));
        familyController.createNewFamily(new Human("Ольга", "Шелест", "29/01/1996", 70),
                new Human("Роман", "Мазур", "25/02/1995", 86));
        familyController.createNewFamily(new Human("Яна", "Тищенко", "12/09/2003", 83),
                new Human("Аркадий", "Зубарев", "02/08/2000", 82));
        familyController.bornChild(familyController.getFamilyByIndex(0), "Ваня", "Юля");
        familyController.bornChild(familyController.getFamilyByIndex(0), "Зорян", "Света");
        familyController.bornChild(familyController.getFamilyByIndex(2), "Олег", "Люда");
        familyController.bornChild(familyController.getFamilyByIndex(2), "Роман", "Катя");
        familyController.bornChild(familyController.getFamilyByIndex(2), "Сема", "Наташа");
        familyController.bornChild(familyController.getFamilyByIndex(3), "Захар", "Надя");
    }

    private static int readNumber(Scanner scanner) {
        int res = 0;
        while (res == 0){
            System.out.println("Введите число:");
            String num = scanner.nextLine();
            try {
                res = Integer.parseInt(num);
            } catch (Exception e) {
                System.out.println("Это не число.");
            }
        }
        return res;
    }
}



