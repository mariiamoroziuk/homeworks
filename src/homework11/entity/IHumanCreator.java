package homework11.entity;

public interface IHumanCreator {
    Human bornChild(String boysName, String girlsName);
}
