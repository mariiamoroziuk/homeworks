package homework3;

import java.util.Scanner;
import java.lang.*;

public class Organizer {
    public static void main(String[] args) {
        String[][] schedules = new String[7][2];
        schedules[0][0] = "SUNDAY";
        schedules[0][1] = "watch a film";
        schedules[1][0] = "MONDAY";
        schedules[1][1] = "go to courses";
        schedules[2][0] = "TUESDAY";
        schedules[2][1] = "do home work";
        schedules[3][0] = "WEDNESDAY";
        schedules[3][1] = "sleep all day";
        schedules[4][0] = "THURSDAY";
        schedules[4][1] = "read the book";
        schedules[5][0] = "FRIDAY";
        schedules[5][1] = "do the whole my work";
        schedules[6][0] = "SATURDAY";
        schedules[6][1] = "dance";

        Scanner scan = new Scanner(System.in);

        while(true) {
            System.out.print("\nPlease, input the day of the week:\n");
            String clientText = scan.nextLine();

            if (clientText.contains("change") || clientText.contains("reschedule")){
                int indexOfDay=-1;
                for (int i=0; i<schedules.length; i++) {
                    if (clientText.toUpperCase().contains(schedules[i][0])) indexOfDay=i;
                }

                if (indexOfDay == -1)System.out.print("\nYou forgot to specify a day\n");
                else{
                    System.out.printf("\nPlease, input new tasks for %s:", schedules[indexOfDay][0].toLowerCase());
                    schedules[indexOfDay][1]=scan.nextLine();
                }
            }
            else {
                switch (clientText.trim().toUpperCase()) {
                    case "SUNDAY":
                        System.out.printf("Your tasks for %s: %s", clientText, schedules[0][1]);
                        break;
                    case "MONDAY":
                        System.out.printf("Your tasks for %s: %s", clientText, schedules[1][1]);
                        break;
                    case "TUESDAY":
                        System.out.printf("Your tasks for %s: %s", clientText, schedules[2][1]);
                        break;
                    case "WEDNESDAY":
                        System.out.printf("Your tasks for %s: %s", clientText, schedules[3][1]);
                        break;
                    case "THURSDAY":
                        System.out.printf("Your tasks for %s: %s", clientText, schedules[4][1]);
                        break;
                    case "FRIDAY":
                        System.out.printf("Your tasks for %s: %s", clientText, schedules[5][1]);
                        break;
                    case "SATURDAY":
                        System.out.printf("Your tasks for %s: %s", clientText, schedules[6][1]);
                        break;
                    case "EXIT":
                        return;
                    default:
                        System.out.println("Sorry, I don't understand you, please try again.");
                }
            }
        }
    }
}
