package homework7;

import homework6.DayOfWeek;

import java.util.Calendar;
import java.util.Objects;
import java.util.Random;

import static java.util.Arrays.copyOf;

public class Family implements IHumanCreator{
    private Human mother;
    private Human father;
    private Human[] children = new Human[0];
    private Pet pet;

    static {
        System.out.println("Загружается новый класс Family");
    }
    {
        System.out.println("Создается новый обьект класса Family");
    }

    Family(Human mother, Human father){
        setMother(mother);
        setFather(father);
        mother.setFamily(this);
        father.setFamily(this);
    }

    public Human getMother(){ return this.mother; }
    public void setMother(Human mother) { this.mother=mother; }

    public Human getFather(){ return this.father; }
    public void setFather(Human father) { this.father=father; }

    public Human[] getChildren(){
        return this.children;
    }
    public void setChildren(Human[] children) { this.children=children; }

    public Pet getPet(){
        return this.pet;
    }
    public void setPet(Pet pet) {
        this.pet=pet;
    }

    void addChild(Human child){
        Human[] children = copyOf(this.getChildren(), this.getChildren().length + 1);
        children[this.getChildren().length] = child;
        this.setChildren(children);
        child.setFamily(this);
    }

    boolean deleteChild(int index){
        if(index > -1 && index < this.getChildren().length){

            this.getChildren()[index].setFamily(null);

            Human[] children = new Human[this.getChildren().length - 1];
            for (int i = 0; i < children.length; i++) {
                if (i < index) children[i] = this.getChildren()[i];
                else children[i] = this.getChildren()[i + 1];
            }

            this.setChildren(children);
            return true;
        } else return false;
    }

    boolean deleteChild(Human child){
        int index=-1;
        for (int i=0; i<this.getChildren().length; i++) {
            if(child.hashCode() == this.getChildren()[i].hashCode() && this.getChildren()[i].equals(child)) index = i;
        }
        return this.deleteChild(index);
    }

    int countFamily(){
        return this.getChildren().length + 2;
    }

    @Override
    public String toString(){
        StringBuilder children = new StringBuilder();
       for(int i=0; i<this.getChildren().length; i++){
           children.append(this.getChildren()[i].toString());
       }
        return "Mother:"+this.getMother().toString()+ "\nFather:"+this.getFather().toString()+ "\nChildren:" +children+"\nPet:" + (this.getPet() != null ? this.getPet().toString() : null);
    }

    @Override
    public boolean equals(Object obj){
        if (this == obj) return true;
        if (!(obj instanceof Family)) return false;

        Family family = (Family)obj;
        return (Objects.equals(this.getMother(), family.getMother()) && Objects.equals(this.getFather(), family.getFather()));
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.getMother(), this.getFather());
    }

    @Override
    protected void finalize() throws Throwable {
        System.out.printf("Deleted object: %s", this.toString());
    }

    @Override
    public Human bornChild() {
        Random random = new Random();
        int randomNumGender = random.nextInt(2);
        int randomNumName = random.nextInt(10);

        String childName = randomNumGender == 0 ? Names.values()[randomNumName+10].name() : Names.values()[randomNumName].name();
        int childIq = (this.mother.getIq() + this.father.getIq()) / 2;
        int thisYear = Calendar.getInstance().get(Calendar.YEAR);
        String[][] childSchedule={{homework6.DayOfWeek.MONDAY.name(), "спать, кушать"},{homework6.DayOfWeek.TUESDAY.name(), "спать, кушать"},{homework6.DayOfWeek.WEDNESDAY.name(), "спать, кушать"},{homework6.DayOfWeek.THURSDAY.name(), "спать, кушать"},{homework6.DayOfWeek.FRIDAY.name(), "спать, кушать"},{homework6.DayOfWeek.SATURDAY.name(), "спать, кушать"},{DayOfWeek.SUNDAY.name(), "спать, кушать"}};

        Human child;
        if(randomNumGender == 0){
            child = new Man(childName, this.father.getSurname(), thisYear, childIq, childSchedule);
        } else {
            child = new Woman(childName, this.father.getSurname(), thisYear, childIq, childSchedule);
        }
        this.addChild(child);
        child.setFamily(this);
        return child;
    }
}
