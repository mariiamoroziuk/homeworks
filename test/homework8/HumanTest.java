package homework8;

import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

class HumanTest {
    Human human = new Human();
    Human human2 = new Human("aa","bb", 1);
    Map<DayOfWeek, String> schedules = new HashMap<>();
    Human human3 = new Human("some","body", 1, 100, schedules);
    Human human4 = human2;
    Human human5 = new Human();
    Dog pet = new Dog( "coco");

    @Test
    void testToString() {
        String result = "Human{name=null, surname=null, year=0, iq=0, schedule={}}";
        assertEquals(result, human.toString());

        String result2 = "Human{name=aa, surname=bb, year=1, iq=0, schedule={}}";
        assertEquals(result2, human2.toString());

        String result3 = "Human{name=aa, surname=bb, year=1, iq=100, schedule={}}";
        assertEquals(result2, human2.toString());
    }

    @Test
    void testEquals() {
        assertTrue(human2.equals(human4));
        assertTrue(!human.equals(human3));
        assertTrue(!human3.equals(human4));
        assertTrue(human.equals(human5));
        assertTrue(!human2.equals(pet));
        assertTrue(!pet.equals(human2));
    }

    @Test
    void testHashCode() {
        assertTrue(human2.hashCode() == human4.hashCode());
        assertTrue(human.hashCode() != human3.hashCode());
        assertTrue(human3.hashCode() != human4.hashCode());
        assertTrue(human.hashCode() == human5.hashCode());
        assertTrue(pet.hashCode() != human3.hashCode());
    }
}