package homework13.entity;

import java.util.Set;

public class DomesticCat extends Pet implements IFoul {
    private Species species = Species.DOMESTIC_CAT;

    public DomesticCat() {
        super();
    }
    public DomesticCat(String nickname) {
        super(nickname);
    }

    public DomesticCat(String nickname, int age, int trickLevel, Set<String> habits){
        super(nickname, age, trickLevel, habits);
    }

    public Species getSpecies(){ return this.species; }

    @Override
    void respond() {
        System.out.printf("Привет, хозяин. Я - %s. Я соскучился!\n", getNickname());
    }

    @Override
    public void foul() {
        System.out.println("Нужно хорошо замести следы...");
    }

}
