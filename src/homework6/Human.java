package homework6;

import java.util.Arrays;
import java.util.Objects;
import java.util.Random;

public class Human {
    private String name;
    private String surname;
    private int year;
    private int iq;
    private String[][] schedule = new String[0][0];
    private Family family;

    static {
        System.out.println("Загружается новый класс Human");
    }
    {
        System.out.println("Создается новый обьект класса Human");
    }

    Human(){
    }
    Human(String name, String surname, int year){
        setName(name);
        setSurname(surname);
        setYear(year);
    }
    Human(String name, String surname, int year, int iq, String[][] schedule){
        this(name, surname, year);
        setIq(iq);
        setSchedule(schedule);
    }

    public String getName(){
        return this.name;
    }
    public void setName(String name) {
        this.name=name;
    }

    public String getSurname(){ return this.surname; }
    public void setSurname(String surname) { this.surname=surname; }

    public int getYear(){ return this.year; }
    public void setYear(int year) { this.year=year; }

    public int getIq(){
        return this.iq;
    }
    public void setIq(int iq) {
        if (iq > 100 || iq < 0) {
            System.out.println("Wrong iq");
            return;
        }
        this.iq=iq;
    }

    public String[][] getSchedule(){
        return this.schedule;
    }
    public void setSchedule(String[][] schedule) {
        this.schedule=schedule;
    }

    public Family getFamily(){ return this.family; }
    public void setFamily(Family family) {
        this.family=family;
    }
//    public void setFamily(String str) { if(str.equals("was deleted"))this.family=null; }

    void greetPet(){
        if(this.getFamily() != null && this.getFamily().getPet() != null)System.out.printf("Привет, %s\n", this.getFamily().getPet().getNickname());
        else System.out.print("So sad that I don't have a pet :(\n");
    }

    void describePet(){
        if(this.getFamily() != null && this.getFamily().getPet() != null)System.out.printf("У меня есть %s, ему %d лет, он %s\n", this.getFamily().getPet().getSpecies(), this.getFamily().getPet().getAge(), this.getFamily().getPet().getTrickLevel()>50 ? "очень хитрый" : "почти не хитрый");
        else System.out.print("I want a dog!!!\n");
    }

    boolean feedPet(boolean flag){
        if(this.getFamily() != null && this.getFamily().getPet() != null){
            if (flag) {
                System.out.printf("Хм... покормлю ка я %s\n", this.getFamily().getPet().getNickname());
                return true;
            } else {
                Random random = new Random();
                int randomNum = random.nextInt(101);
                if (this.getFamily().getPet().getTrickLevel() > randomNum) {
                    System.out.printf("Хм... покормлю ка я %s\n", this.getFamily().getPet().getNickname());
                    return true;
                } else {
                    System.out.printf("Думаю, %s не голоден.\n", this.getFamily().getPet().getNickname());
                    return false;
                }
            }
        }else return false;
    }

    @Override
    public String toString(){
        return "Human{name="+this.getName()+ ", surname="+this.getSurname()+ ", year="+this.getYear()+ ", iq="+this.getIq()+", schedule="+ Arrays.deepToString(this.getSchedule())+"}";
    }

    @Override
    public boolean equals(Object obj){
        if (this == obj) return true;
        if (!(obj instanceof Human)) return false;

        Human human = (Human)obj;
        return (Objects.equals(getName(), human.getName()) && Objects.equals(getSurname(), human.getSurname()));
    }

    @Override
    public int hashCode() {
        return Objects.hash(getName(), getSurname());
    }

    @Override
    protected void finalize() throws Throwable {
        System.out.printf("Deleted object: %s", this.toString());
    }
}
