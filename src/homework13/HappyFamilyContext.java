package homework13;

import homework13.controller.FamilyController;
import homework13.dao.CollectionFamilyDao;
import homework13.dao.IFamilyDao;
import homework13.service.FamilyService;

public class HappyFamilyContext {
    public static FamilyController familyController;

    public static void init() {
        IFamilyDao familyDao = new CollectionFamilyDao();

        FamilyService familyService = new FamilyService(familyDao);
        familyController = new FamilyController(familyService);

    }
}