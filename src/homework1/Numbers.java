package homework1;

import java.util.Arrays;
import java.util.Collections;
import java.util.Random;
import java.util.Scanner;

public class Numbers {
    public static void main(String[] args) {
        Random random = new Random();
        Scanner scan = new Scanner(System.in);

        int myNum=random.nextInt(101);
        int yourNum=-1, count=0;
        Integer[] yourNumbers = new Integer[101];

        System.out.print("Enter your name: ");
        String name = scan.nextLine();

        System.out.println("\nLet the game begin!\n");
        System.out.print("Enter any number from 0 to 100: ");

        do{
            String string = scan.nextLine();

            if(string.matches("\\d+")){
                yourNum = Integer.parseInt(string);

                if(yourNum<0||yourNum>100) {
                    System.out.print("Your number must be from 0 to 100. Please, try again: ");
                    continue;
                }

                if(count>0){
                    boolean repeatNum = false;
                    for (int i = 0; i < count; i++) {
                        if (yourNumbers[i] == yourNum) {
                            repeatNum = true;
                            break;
                        }
                    }
                    if (repeatNum) {
                        System.out.print("You tried this number already. Please, enter another number: ");
                        continue;
                    }
                }

                if(yourNum<myNum){
                    System.out.print("Your number is too small. Please, try again: ");
                }

                if(myNum<yourNum){
                    System.out.print("Your number is too big. Please, try again: ");
                }
                yourNumbers[count]=yourNum;
                count++;
            } else System.out.print("It does not an integer. Please, try again: ");

        }while(yourNum != myNum);

        Integer[] result = new Integer[count];
        System.arraycopy(yourNumbers, 0, result, 0, result.length);
        Arrays.sort(result, Collections.reverseOrder());

        System.out.printf("\nCongratulations, %s ! \n\nYour numbers: ", name);
        for (Integer item : result) {
            System.out.printf("%d ",item);
        }
    }
}
