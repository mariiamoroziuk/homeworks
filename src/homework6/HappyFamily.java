package homework6;

public class HappyFamily {
    public static void main(String[] args) {
        String[] petHabits={"кушать", "спать", "гулять"};
        Pet petty=new Pet(Species.TURTLE, "колобок", 2, 80, petHabits);

        Human mom=new Human();
        mom.setName("Настя");
        mom.setSurname("Каменских");

        Human dad=new Human("Алексей", "Потапенко", 1980);

        String[][] sonSchedule={{DayOfWeek.MONDAY.name(), "в школу"},{DayOfWeek.TUESDAY.name(), "в школу"},{DayOfWeek.WEDNESDAY.name(), "в школу"},{DayOfWeek.THURSDAY.name(), "в школу"},{DayOfWeek.FRIDAY.name(), "в школу"},{DayOfWeek.SATURDAY.name(), "гулять"},{DayOfWeek.SUNDAY.name(), "гулять"}};
        Human son=new Human("Дмитрий", "Монатик", 1990,  100, sonSchedule);
        Human daughter=new Human("Алена", "Алена", 1996,  100, sonSchedule);

        Family family=new Family(mom,dad);
        family.addChild(son);
        family.addChild(daughter);

        petty.eat();
        petty.respond();
        petty.foul();
        System.out.println(petty.toString());

        son.greetPet();
        son.describePet();
        son.feedPet(false);
        System.out.println(son.toString());

        System.out.println(family.toString());
        System.out.println(family.countFamily());

//        int i=0;
//        while(i < 100000){Human human = new Human(); i++;}
        Pet pet1 = new Pet();
        System.out.println(pet1);

        family.deleteChild(1);
    }
}
