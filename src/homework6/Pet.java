package homework6;

import java.util.Arrays;
import java.util.Objects;

public class Pet {
    private Species species;
    private String nickname;
    private int age;
    private int trickLevel;
    private String[] habits;

    static {
        System.out.println("Загружается новый класс Pet");
    }
    {
        System.out.println("Создается новый обьект класса Pet");
    }

    Pet(){
    }
    Pet(Species species, String nickname){
        setSpecies(species);
        setNickname(nickname);
    }
    Pet(Species species, String nickname, int age, int trickLevel, String[] habits){
        this(species, nickname);
        setAge(age);
        setTrickLevel(trickLevel);
        setHabits(habits);
    }

    public Species getSpecies(){ return this.species; }
    public void setSpecies(Species species) { this.species=species; }

    public String getNickname(){
        return this.nickname;
    }
    public void setNickname(String nickname) {
        this.nickname=nickname;
    }

    public int getAge(){
        return this.age;
    }
    public void setAge(int age) {
        this.age=age;
    }

    public int getTrickLevel(){
        return this.trickLevel;
    }
    public void setTrickLevel(int trickLevel) {
        if (trickLevel > 100||trickLevel < 0) {
            System.out.println("Wrong trickLevel");
            return;
        }
        this.trickLevel=trickLevel;
    }

    public String[] getHabits(){
        return this.habits;
    }
    public void setHabits(String[] habits) {
        this.habits=habits;
    }

    void eat(){
        System.out.println("Я кушаю!");
    }

    void respond(){
        System.out.printf("Привет, хозяин. Я - %s. Я соскучился!\n", getNickname());
    }

    void foul(){
        System.out.println("Нужно хорошо замести следы...");
    }

    @Override
    public String toString(){
        return this.getSpecies()+"{nickname="+this.getNickname()+", age="+this.getAge()+", trickLevel="+this.getTrickLevel()+", habits="+Arrays.toString(this.getHabits())
                +", canFly="+(this.getSpecies() != null ? this.getSpecies().getCanFly() : null)+", numberOfLegs="+(this.getSpecies() != null ? this.getSpecies().getNumberOfLegs() : null)+
                ", hasFur="+(this.getSpecies() != null ? this.getSpecies().getHasFur() : null)+"}";
    }

    @Override
    public boolean equals(Object obj){
        if (this == obj)
            return true;
        if (!(obj instanceof Pet))
            return false;
        Pet pet = (Pet)obj;
        return (Objects.equals(this.getSpecies(), pet.getSpecies()) && Objects.equals(this.getNickname(), pet.getNickname()));
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.getSpecies(), this.getNickname());
    }

    @Override
    protected void finalize() throws Throwable {
        System.out.printf("Deleted object: %s", this.toString());
    }

}
