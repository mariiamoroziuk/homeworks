package homework4;

public class HappyFamily {
    public static void main(String[] args) {
        String[] petHabits={"кушать", "спать", "гулять"};
        Pet petty=new Pet("черепаха", "колобок", 2, 80, petHabits);

        Human mom=new Human();
        mom.setName("Настя");
        mom.setSurname("Каменских");

        Human dad=new Human("Алексей", "Потапенко", 1980);

        String[][] sonSchedule={{"понедельник", "в школу"},{"вторник", "в школу"},{"среда", "в школу"},{"четверг", "в школу"},{"пятница", "в школу"},{"суббота", "гулять"},{"воскресенье", "гулять"}};
        Human son=new Human("Дмитрий", "Монатик", 1990,  100, sonSchedule);
        Human daughter=new Human("Алена", "Алена", 1996,  100, sonSchedule);

        Family family=new Family(mom,dad);
        family.addChild(son);
        family.addChild(daughter);

        petty.eat();
        petty.respond();
        petty.foul();
        System.out.println(petty.toString());

        son.greetPet();
        son.describePet();
        son.feedPet(false);
        System.out.println(son.toString());

        System.out.println(family.toString());
        System.out.println(family.countFamily());

    }
}
