package homework13.dao;

import homework13.entity.*;

import java.io.IOException;
import java.util.List;

public interface IFamilyDao {
    List<Family> getAllFamilies();

    Family getFamilyByIndex (int index);

    Family getFamilyById (long id);

    boolean deleteFamily (int index);

    boolean deleteFamily (Family family);

    void saveFamily (Family family);

    void loadDataToDB(List<Family> families) throws IOException;

    void loadDataFromDB() throws IOException;
}
