package homework11.entity;

import java.util.Map;

public final class Man extends Human {

    public Man() {
        super();
    }
    public Man(String name, String surname, int year) {
        super(name, surname, year);
    }

    public Man(String name, String surname, int year, int iq, Map<DayOfWeek, String> schedule){
        super(name, surname, year, iq, schedule);
    }

    @Override
    void greetPet(){
        if(this.pet != null)System.out.printf("Привет, нигадяй %s\n", this.pet.getNickname());
        else System.out.print("So sad that I don't have a pet :(\n");
    }

    void repairCar(){
        System.out.println("Я чиню свою машину)))\n");
    }
}
