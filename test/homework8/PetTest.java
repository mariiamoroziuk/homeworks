package homework8;

import org.junit.jupiter.api.Test;

import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

class PetTest {
    Dog pet = new Dog();
    Dog pet2 = new Dog( "coco");
    Set<String> petHabits = new HashSet<>();
    Dog pet3 = new Dog("someone", 2, 5, petHabits);
    Dog pet4 = pet2;
    Dog pet5 = new Dog();
    Human human = new Human("aa","bb", 1);

    @Test
    void testToString() {
        String result = "DOG{nickname=null, age=0, trickLevel=0, habits=[], canFly=false, numberOfLegs=4, hasFur=true}";
        assertEquals(result, pet.toString());

        String result2 = "DOG{nickname=coco, age=0, trickLevel=0, habits=[], canFly=false, numberOfLegs=4, hasFur=true}";
        assertEquals(result2, pet2.toString());

        String result3 = "DOG{nickname=someone, age=2, trickLevel=5, habits=[], canFly=false, numberOfLegs=4, hasFur=true}";
        assertEquals(result3, pet3.toString());
    }

    @Test
    void testEquals() {
        assertTrue(pet2.equals(pet4));
        assertTrue(!pet.equals(pet3));
        assertTrue(!pet3.equals(pet4));
        assertTrue(pet.equals(pet5));
        assertTrue(!human.equals(pet5));
        assertTrue(!pet.equals(human));
    }

    @Test
    void testHashCode() {
        assertTrue(pet2.hashCode() == pet4.hashCode());
        assertTrue(pet.hashCode() != pet3.hashCode());
        assertTrue(pet3.hashCode() != pet4.hashCode());
        assertTrue(pet.hashCode() == pet5.hashCode());
        assertTrue(pet2.hashCode() != human.hashCode());
    }
}