package homework10.dao;

import homework10.entity.*;

import java.util.ArrayList;
import java.util.List;

public class CollectionFamilyDao implements IFamilyDao {

    private final List<Family> allFamilies = new ArrayList<>();
    private long idCounter = 1;

    @Override
    public List<Family> getAllFamilies() {
        return this.allFamilies;
    }

    @Override
    public Family getFamilyByIndex(int index) {
        if(index < 0 || index > this.allFamilies.size() - 1) {
            return null;
        } else {
            return this.allFamilies.get(index);
        }
    }

    @Override
    public Family getFamilyById(long id) {
        Family res = null;
        for (int i = 0; i < this.allFamilies.size(); i++) {
            if (this.allFamilies.get(i).getId() == id) res = this.allFamilies.get(i);
        }
        return res;
    }

    @Override
    public boolean deleteFamily(int index) {
        if(index < 0 || index > this.allFamilies.size() - 1) {
            return false;
        } else {
            return deleteFamily(this.allFamilies.get(index));
        }
    }

    @Override
    public boolean deleteFamily(Family family) {
        return this.allFamilies.remove(family);
    }

    @Override
    public void saveFamily(Family family) {
        int index = this.allFamilies.indexOf(family);

        family.setId(idCounter++);

        if(index > -1){
            this.allFamilies.set(index, family);
        } else {
            this.allFamilies.add(family);
        }

    }

}
