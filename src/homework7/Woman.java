package homework7;

public class Woman extends Human{
    Woman() {
        super();
    }
    Woman(String name, String surname, int year) {
        super(name, surname, year);
    }

    Woman(String name, String surname, int year, int iq, String[][] schedule){
        super(name, surname, year, iq, schedule);
    }

    @Override
    void greetPet(){
        if(this.getFamily() != null && this.getFamily().getPet() != null)System.out.printf("Привет, золотце %s\n", this.getFamily().getPet().getNickname());
        else System.out.print("So sad that I don't have a pet :(\n");
    }

    void makeup(){
        System.out.println("Я крашу губы ;)\n");
    }
}
