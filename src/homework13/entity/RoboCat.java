package homework13.entity;

import java.util.Set;

public class RoboCat extends Pet {
    private Species species = Species.ROBO_CAT;

    public RoboCat() {
        super();
    }
    public RoboCat(String nickname) {
        super(nickname);
    }

    public RoboCat(String nickname, int age, int trickLevel, Set<String> habits){
        super(nickname, age, trickLevel, habits);
    }

    public Species getSpecies(){ return this.species; }

    @Override
    void respond() {
        System.out.printf("Привет, хозяин. Я - %s. Я соскучился!\n", getNickname());
    }
}
