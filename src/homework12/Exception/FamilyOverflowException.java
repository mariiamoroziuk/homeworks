package homework12.Exception;

public class FamilyOverflowException extends RuntimeException{
    public FamilyOverflowException(int familyCount){
        super(String.format("This family bigger than %d, ", familyCount));
    }
}
