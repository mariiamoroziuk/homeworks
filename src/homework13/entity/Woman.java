package homework13.entity;

import java.io.Serializable;
import java.util.Map;

public class Woman extends Human implements Serializable {
    public Woman() {
        super();
    }
    public Woman(String name, String surname, long birthDay) {
        super(name, surname, birthDay);
    }

    public Woman(String name, String surname, long birthDay, int iq, Map<DayOfWeek, String> schedule){
        super(name, surname, birthDay, iq, schedule);
    }

    @Override
    void greetPet(){
        if(this.pet != null)System.out.printf("Привет, золотце %s\n", this.pet.getNickname());
        else System.out.print("So sad that I don't have a pet :(\n");
    }

    void makeup(){
        System.out.println("Я крашу губы ;)\n");
    }
}
