package homework13.exception;

public class FamilyOverflowException extends RuntimeException{
    public FamilyOverflowException(int familyCount){
        super(String.format("This family bigger than %d, ", familyCount));
    }
}
