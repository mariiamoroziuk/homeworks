package homework9.service;

import homework8.*;
import homework9.controller.FamilyController;
import homework9.dao.CollectionFamilyDao;
import homework9.dao.IFamilyDao;
import org.junit.jupiter.api.Test;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertTrue;

class FamilyServiceTest {
    Set<String> petHabits = new HashSet<>();
    Map<DayOfWeek, String> schedule = new HashMap<>();
    Dog petty=new Dog("колобок", 2, 80, petHabits);
    Fish petty2=new Fish("zzzzoommmm", 2, 80, petHabits);

    Man man = new Man("Алексей", "Потапенко", 1980, 80, schedule);
    Woman woman = new Woman("Настя", "Каменских", 1990, 70, schedule);
    Man man2 = new Man("Костик", "Ильченко", 1980, 85, schedule);
    Woman woman2 = new Woman("Вика", "Русак", 1990, 92, schedule);
    Man man3 = new Man("Some", "Gay", 1989, 90, schedule);
    Woman woman3 = new Woman("Some", "Girl", 1990, 92, schedule);
    Man man4 = new Man("Ваня", "Иванов", 2018, 35, schedule);
    Family family = new Family(woman, man);
    Family family2 = new Family(woman2, man2);

    List<Family> familyList = new ArrayList<>();

    @Test
    void getAllFamilies() {
        FamilyService familyService = new FamilyService();
        familyList.add(family);
        familyService.createNewFamily(woman, man);
        assertEquals(familyList.size(), familyService.getAllFamilies().size());
        assertEquals(familyList.get(0), familyService.getAllFamilies().get(0));
        familyList.add(family2);
        familyService.createNewFamily(woman2, man2);
        assertEquals(familyList.size(), familyService.getAllFamilies().size());
        assertEquals(familyList.get(1), familyService.getAllFamilies().get(1));
    }

    @Test
    void getFamiliesBiggerThan() {
        FamilyService familyService = new FamilyService();
        familyService.createNewFamily(woman, man);
        familyService.createNewFamily(woman2, man2);
        familyService.createNewFamily(woman3, man3);
        assertEquals(0, familyService.getFamiliesBiggerThan(2).size());
        assertEquals(0, familyService.getFamiliesBiggerThan(3).size());
        assertEquals(3, familyService.getFamiliesBiggerThan(1).size());
    }

    @Test
    void getFamiliesLessThan() {
        FamilyService familyService = new FamilyService();
        familyService.createNewFamily(woman, man);
        familyService.createNewFamily(woman2, man2);
        familyService.createNewFamily(woman3, man3);
        assertEquals(0, familyService.getFamiliesLessThan(2).size());
        assertEquals(3, familyService.getFamiliesLessThan(3).size());
        assertEquals(0, familyService.getFamiliesLessThan(1).size());
    }

    @Test
    void countFamiliesWithMemberNumber() {
        FamilyService familyService = new FamilyService();
        familyService.createNewFamily(woman, man);
        familyService.createNewFamily(woman2, man2);
        familyService.createNewFamily(woman3, man3);
        assertEquals(3, familyService.countFamiliesWithMemberNumber(2));
        assertEquals(0, familyService.countFamiliesWithMemberNumber(3));
        assertEquals(0, familyService.countFamiliesWithMemberNumber(1));
    }

    @Test
    void deleteFamilyByIndex() {
        FamilyService familyService = new FamilyService();
        familyService.createNewFamily(woman, man);
        familyService.createNewFamily(woman2, man2);
        familyService.createNewFamily(woman3, man3);
        assertFalse(familyService.deleteFamilyByIndex(4));
        assertTrue(familyService.deleteFamilyByIndex(2));
        assertFalse(familyService.deleteFamilyByIndex(2));
    }

    @Test
    void bornChild() {
        FamilyService familyService = new FamilyService();
        familyService.createNewFamily(woman, man);
        assertEquals(woman.getFamily(), familyService.bornChild(woman.getFamily(), "Tom", "Jerry"));
        assertEquals(woman.getFamily(), familyService.bornChild(woman.getFamily(), "lulu", "Sam"));
    }

    @Test
    void adoptChild() {
        FamilyService familyService = new FamilyService();
        familyService.createNewFamily(woman, man);
        assertEquals(woman.getFamily(), familyService.adoptChild(woman.getFamily(), man2));
        assertEquals(woman.getFamily(), familyService.adoptChild(woman.getFamily(), woman2));
    }

    @Test
    void count() {
        FamilyService familyService = new FamilyService();
        familyService.createNewFamily(woman, man);
        assertEquals(1, familyService.count());
        familyService.createNewFamily(woman2, man2);
        assertEquals(2, familyService.count());
        familyService.createNewFamily(woman3, man3);
        assertEquals(3, familyService.count());
    }

    @Test
    void getFamilyById() {
        FamilyService familyService = new FamilyService();
        familyService.createNewFamily(woman, man);
        familyService.createNewFamily(woman2, man2);
        familyService.createNewFamily(woman3, man3);
        assertEquals(woman.getFamily(), familyService.getFamilyById(1));
        assertEquals(woman2.getFamily(), familyService.getFamilyById(2));
        assertEquals(woman3.getFamily(), familyService.getFamilyById(3));
    }

    @Test
    void getPets() {
        FamilyService familyService = new FamilyService();
        familyService.createNewFamily(woman, man);
        familyService.createNewFamily(woman2, man2);
        familyService.getFamilyById(1).addPet(petty);
        Set<Pet> pets = new HashSet<>();
        pets.add(petty);
        assertEquals(pets, familyService.getPets(0));
        familyService.getFamilyById(1).addPet(petty2);
        pets.add(petty2);
        assertEquals(pets, familyService.getPets(0));
        assertEquals(new HashSet<>(), familyService.getPets(1));
    }

    @Test
    void addPet() {
        FamilyService familyService = new FamilyService();
        familyService.createNewFamily(woman, man);
        assertEquals(0, woman.getFamily().getPets().size());
        familyService.addPet(0, petty);
        assertEquals(1, woman.getFamily().getPets().size());
        familyService.addPet(0, petty2);
        assertEquals(2, woman.getFamily().getPets().size());
    }
}