package homework13.dao;

import homework13.entity.Family;
import homework13.logger.FamilyLogger;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class Database {
    private static String path = "database.txt";

    public static void write(List<Family> families) {
        try(ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(path)))
        {
            oos.writeObject(families);
        }
        catch(Exception e){
            FamilyLogger.error("запись списка семей в файл");
            e.printStackTrace();
        }
    }
    public static List<Family> read() {
        List<Family> families = new ArrayList<Family>();
        try(ObjectInputStream ois = new ObjectInputStream(new FileInputStream(path)))
        {
            families=((List<Family>)ois.readObject());
        }
        catch(Exception e){
            FamilyLogger.error("считывание списка семей с файла");
            e.printStackTrace();
        }
        return families;
    }

}