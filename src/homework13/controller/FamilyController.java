package homework13.controller;

import homework13.exception.*;
import homework13.entity.*;
import homework13.logger.FamilyLogger;
import homework13.service.*;

import java.io.IOException;
import java.util.List;
import java.util.Set;

public class FamilyController {
    private FamilyService familyService;

    public FamilyController(FamilyService familyService) {
        this.familyService = familyService;
    }

    public List<Family> getAllFamilies(){
        return this.familyService.getAllFamilies();
    }

    public void displayAllFamilies() {
        this.familyService.displayAllFamilies();
    }

    public List<Family> getFamiliesBiggerThan(int num){
        return this.familyService.getFamiliesBiggerThan(num);
    }

    public List<Family> getFamiliesLessThan(int num){
        return this.familyService.getFamiliesLessThan(num);
    }

    public int countFamiliesWithMemberNumber(int num){
        return this.familyService.countFamiliesWithMemberNumber(num);
    }

    public void createNewFamily(Human mother, Human father){
        this.familyService.createNewFamily(mother, father);
    }

    public boolean deleteFamilyByIndex(int index){
        return this.familyService.deleteFamilyByIndex(index);
    }

    public Family bornChild(Family family, String boysName, String girlsName){
        int limit = 5;
        if (family.countFamily() > limit) {
            FamilyLogger.error("рождение ребенка: привышения лимита детей в семье");
            throw new FamilyOverflowException(limit);
        } else {
            return this.familyService.bornChild(family, boysName, girlsName);
        }
    }

    public Family adoptChild(Family family, Human child) {
        int limit = 5;
        if (family.countFamily() > limit) {
            FamilyLogger.error("усыновление ребенка: привышения лимита детей в семье");
            throw new FamilyOverflowException(limit);
        } else {
            return this.familyService.adoptChild(family, child);
        }
    }

    public void deleteAllChildrenOlderThen(int age) {
        this.familyService.deleteAllChildrenOlderThen(age);
    }

    public int count(){
        return this.familyService.count();
    }

    public Family getFamilyById(long id){
        return this.familyService.getFamilyById(id);
    }

    public Family getFamilyByIndex(int index){
        return this.familyService.getFamilyByIndex(index);
    }

    public Set<Pet> getPets(int index){
        return familyService.getPets(index);
    }

    public void addPet(int index, Pet pet){
        this.familyService.addPet(index, pet);
    }

    public void loadDataToDB(List<Family> families) throws IOException {
        this.familyService.loadDataToDB(families);
    }
    public void loadDataFromDB() throws IOException {
        this.familyService.loadDataFromDB();
    }
}
