package homework7;

public final class Man extends Human{

    Man() {
        super();
    }
    Man(String name, String surname, int year) {
        super(name, surname, year);
    }

    Man(String name, String surname, int year, int iq, String[][] schedule){
        super(name, surname, year, iq, schedule);
    }

    @Override
    void greetPet(){
        if(this.getFamily() != null && this.getFamily().getPet() != null)System.out.printf("Привет, нигадяй %s\n", this.getFamily().getPet().getNickname());
        else System.out.print("So sad that I don't have a pet :(\n");
    }

    void repairCar(){
        System.out.println("Я чиню свою машину)))\n");
    }
}
