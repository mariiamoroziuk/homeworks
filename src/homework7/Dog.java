package homework7;

import java.util.Arrays;

public class Dog extends Pet implements IFoul{

    private Species species = Species.DOG;

    Dog() {
        super();
    }
    Dog(String nickname) {
        super(nickname);
    }

    Dog(String nickname, int age, int trickLevel, String[] habits){
        super(nickname, age, trickLevel, habits);
    }

    public Species getSpecies(){ return this.species; }

    @Override
    void respond(){
        System.out.printf("Привет, хозяин. Я - %s. Я соскучился!\n", this.getNickname());
    }

    @Override
    public void foul() {
        System.out.println("Нужно хорошо замести следы...");
    }
}
