package homework7;

public class Fish extends Pet {
    private Species species = Species.FISH;

    Fish() {
        super();
    }
    Fish(String nickname) {
        super(nickname);
    }

    Fish(String nickname, int age, int trickLevel, String[] habits){
        super(nickname, age, trickLevel, habits);
    }

    public Species getSpecies(){ return this.species; }

    @Override
    void respond(){
        System.out.printf("Привет, хозяин. Я - %s. Я соскучился!\n", getNickname());
    }
}
