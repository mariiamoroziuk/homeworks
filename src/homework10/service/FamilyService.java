package homework10.service;

import homework10.entity.*;
import homework10.dao.CollectionFamilyDao;
import homework10.dao.IFamilyDao;

import java.time.LocalDate;
import java.time.Period;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Set;

public class FamilyService {
    private IFamilyDao familyDao = new CollectionFamilyDao();

    public List<Family> getAllFamilies(){
        return this.familyDao.getAllFamilies();
    }

    public void displayAllFamilies(){
        for (Family family : this.familyDao.getAllFamilies()) {
            System.out.println(family.toString());
        }
    }

    public List<Family> getFamiliesBiggerThan(int num){
        List<Family> result = new ArrayList<>();
        for (Family family : this.familyDao.getAllFamilies()) {
            if (family.countFamily() > num) {
                result.add(family);
                System.out.println(family.toString());
            }
        }
        return result;
    }

    public List<Family> getFamiliesLessThan(int num){
        List<Family> result = new ArrayList<>();
        for (Family family : this.familyDao.getAllFamilies()) {
            if (family.countFamily() < num) {
                result.add(family);
                System.out.println(family.toString());
            }
        }
        return result;
    }

    public int countFamiliesWithMemberNumber(int num){
        int result = 0;
        for (Family family : this.familyDao.getAllFamilies()) {
            if (family.countFamily() == num) {
                result++;
            }
        }
        return result;
    }

    public void createNewFamily(Human mother, Human father){
        Family family = new Family(mother, father);
        this.familyDao.saveFamily(family);
    }

    public boolean deleteFamilyByIndex(int index){
        return this.familyDao.deleteFamily(index);
    }

    public Family bornChild(Family family, String boysName, String girlsName){
        family.bornChild(boysName, girlsName);
        this.familyDao.saveFamily(family);
        return family;
    }

    public Family adoptChild(Family family, Human child) {
        family.addChild(child);
        this.familyDao.saveFamily(family);
        return family;
    }

    public void deleteAllChildrenOlderThen(int age) {
        LocalDate today = LocalDate.now();
        for (Family family : this.familyDao.getAllFamilies()) {
            for (Human child : family.getChildren()) {
                LocalDate birthday = LocalDate.ofEpochDay(child.getBirthDate());
                Period p = Period.between(birthday, today);
                if (p.getYears() > age) {
                    family.deleteChild(child);
                    this.familyDao.saveFamily(family);
                }
            }
        }
    }

    public int count(){
        return this.familyDao.getAllFamilies().size();
    }

    public Family getFamilyById(long id){
        return this.familyDao.getFamilyById(id);
    }

    public Set<Pet> getPets(int index){
        return this.familyDao.getFamilyByIndex(index).getPets();
    }

    public void addPet(int index, Pet pet){
        Family family = this.familyDao.getFamilyByIndex(index);
        family.addPet(pet);
        this.familyDao.saveFamily(family);
    }

}
