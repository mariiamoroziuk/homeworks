package homework4;

import java.util.Objects;
import static java.util.Arrays.*;

public class Family {
    private Human mother;
    private Human father;
    private Human[] children = new Human[0];
    private Pet pet;

    static {
        System.out.println("Загружается новый класс Family");
    }
    {
        System.out.println("Создается новый обьект класса Family");
    }

    Family(Human mother, Human father){
        setMother(mother);
        setFather(father);
        mother.setFamily(this);
        father.setFamily(this);
    }

    public Human getMother(){ return this.mother; }
    public void setMother(Human mother) { this.mother=mother; }

    public Human getFather(){ return this.father; }
    public void setFather(Human father) { this.father=father; }

    public Human[] getChildren(){
        return this.children;
    }
    public void setChildren(Human[] children) { this.children=children; }

    public Pet getPet(){
        return this.pet;
    }
    public void setPet(Pet pet) {
        this.pet=pet;
    }

    void addChild(Human child){
        Human[] children = copyOf(this.getChildren(), this.getChildren().length +1);
        children[this.getChildren().length] = child;
        this.setChildren(children);

        child.setFamily(this);
    }

    boolean deleteChild(int index){
        if(index > -1 && index < this.getChildren().length){
            Human[] children = new Human[this.getChildren().length - 1];
            for (int i = 0; i < children.length; i++) {
                if (i < index) children[i] = this.getChildren()[i];
                else children[i] = this.getChildren()[i + 1];
            }

            this.setChildren(children);
            this.getChildren()[index].setFamily(null);
            return true;
        } else return false;
    }

    boolean deleteChild(Human child){
        int index=-1;
        for (int i=0; i<this.getChildren().length; i++) {
            if(child.hashCode() == this.getChildren()[i].hashCode() && this.getChildren()[i].equals(child)) index = i;
        }
        return this.deleteChild(index);
    }

    int countFamily(){
        return this.getChildren().length + 2;
    }

    @Override
    public String toString(){
        StringBuilder children = new StringBuilder();
            for (Human child : this.getChildren()) {
                children.append(child.toString());
            }
        return "Mother:"+this.getMother().toString()+ "\nFather:"+this.getFather().toString()+ "\nChildren:" +children+"\nPet:" + (this.getPet() != null ? this.getPet().toString() : null);
    }

    @Override
    public boolean equals(Object obj){
        if (this == obj)
            return true;
        if (!(obj instanceof Family))
            return false;
        Family family = (Family)obj;
        return (Objects.equals(this.getMother(), family.getMother()) && Objects.equals(this.getFather(), family.getFather()));
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.getMother(), this.getFather());
    }
}
