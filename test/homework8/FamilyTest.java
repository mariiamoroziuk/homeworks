package homework8;

import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

class FamilyTest {
    Human human1 = new Human("man","husband", 28);
    Map<DayOfWeek, String> schedules= new HashMap<>();
    Human human2 = new Human("mom","wife", 28);
    Human human3 = new Human();
    Human human4 = new Human("woman","wife", 28, 100, schedules);
    Human human5 = new Human("еее","ттт", 20);
    Family family = new Family(human2, human1);
    Family family2 = new Family(human2, human3);
    Family family3 = family;
    Family family4 = new Family(human1, human2);
    Family family5 = new Family(human4, human3);

    @Test
    void testDeleteChildByIndex() {
        family.addChild(human4);
        family.addChild(human3);
        family.addChild(human5);
        assertTrue(!family.deleteChild(3));
        assertEquals(3, family.getChildren().size());
        assertTrue(!family.deleteChild(-2));
        assertEquals(3, family.getChildren().size());
        assertTrue(family.deleteChild(1));
        assertEquals(2, family.getChildren().size());
    }
    @Test
    void testDeleteChild() {
        family.addChild(human4);
        family.addChild(human3);
        family.addChild(human5);
        assertTrue(!family.deleteChild(human1));
        assertEquals(3, family.getChildren().size());
        assertTrue(!family.deleteChild(human2));
        assertEquals(3, family.getChildren().size());
        assertTrue(family.deleteChild(human3));
        assertEquals(2, family.getChildren().size());
    }
    @Test
    void testAddChild() {
        family.addChild(human4);
        assertEquals(1, family.getChildren().size());
        assertEquals(human4, family.getChildren().get(family.getChildren().size()-1));
        family.addChild(human3);
        assertEquals(2, family.getChildren().size());
        assertEquals(human3, family.getChildren().get(family.getChildren().size()-1));
        family.addChild(human5);
        assertEquals(3, family.getChildren().size());
        assertEquals(human5, family.getChildren().get(family.getChildren().size()-1));
    }

    @Test
    void testCountFamily() {
        family.addChild(human4);
        family.addChild(human3);
        assertEquals(4, family.countFamily());
        family.deleteChild(1);
        assertEquals(3, family.countFamily());
    }

    @Test
    void testToString() {
        String result = "Mother:Human{name=mom, surname=wife, year=28, iq=0, schedule={}}\n" +
                "Father:Human{name=man, surname=husband, year=28, iq=0, schedule={}}\n" +
                "Children:[]\n" +
                "Pets:[]";
        assertEquals(result, family.toString());
        String result2 = "Mother:Human{name=mom, surname=wife, year=28, iq=0, schedule={}}\n" +
                "Father:Human{name=null, surname=null, year=0, iq=0, schedule={}}\n" +
                "Children:[]\n" +
                "Pets:[]";
        assertEquals(result2, family2.toString());
    }

    @Test
    void testEquals() {
        assertTrue(!family2.equals(family));
        assertTrue(family3.equals(family));
        assertTrue(!family5.equals(family4));
        assertTrue(!family.equals(family4));
        assertTrue(!human2.equals(family));
    }

    @Test
    void testHashCode() {
        assertTrue(family2.hashCode() != family.hashCode());
        assertTrue(family3.hashCode() == family.hashCode());
        assertTrue(family5.hashCode() != family4.hashCode());
        assertTrue(family.hashCode() != family4.hashCode());
        assertTrue(human1.hashCode() != family.hashCode());
    }
}