package homework2;

import java.util.Random;
import java.util.Scanner;

public class shooting {
    public static void main(String[] args) {
        Random random = new Random();
        Scanner scan = new Scanner(System.in);

        int targetRow=random.nextInt(4);
        int targetColumn=random.nextInt(4);

        int[][] ship = new int[3][2];
        if (targetRow%2==0){
            for (int i = 0; i < 3; i++){
                ship[i][0]=targetRow> 2 ? targetRow-i: targetRow+i;
                ship[i][1]=targetColumn;
            }
        } else {
            for (int i = 0; i < 3; i++) {
                ship[i][0] = targetRow;
                ship[i][1] = targetColumn > 2 ? targetColumn - i : targetColumn + i;
            }
        }

        int checkWin=ship.length;

        char[][] area= new char[5][5];
        for (int i = 0; i < 5; i++) {
            for (int j = 0; j < 5; j++) {
                area[i][j]='-';
            }
        }

        System.out.println("\nAll set. Get ready to rumble!\n");

        do {
            String clientRow="", clientColumn="";

            while (numValidation(clientRow)) {
                System.out.print("Enter your number of row from 1 to 5: ");
                clientRow = scan.nextLine();
            }
            while (numValidation(clientColumn)) {
                System.out.print("Enter your number of column from 1 to 5: ");
                clientColumn = scan.nextLine();
            }

            if (area[Integer.parseInt(clientRow) - 1][Integer.parseInt(clientColumn) - 1] != 'x'){
                if (hit(Integer.parseInt(clientRow) - 1, Integer.parseInt(clientColumn) - 1, ship)) {
                    area[Integer.parseInt(clientRow) - 1][Integer.parseInt(clientColumn) - 1] = 'x';
                    checkWin--;
                    if (checkWin == 0) System.out.println("\nYou have won!\n");
                } else area[Integer.parseInt(clientRow) - 1][Integer.parseInt(clientColumn) - 1] = '*';
            }

            System.out.println("\t" + 1 + "\t" + 2 + "\t" + 3 + "\t" + 4 + "\t" + 5);
            for (int i = 0; i < 5; i++) {
                System.out.print(i + 1 + "\t");
                for (int j = 0; j < 5; j++) {
                    System.out.print(area[i][j] + "\t");
                }
                System.out.println();
            }
        } while(checkWin!=0);

    }
    static boolean numValidation(String string){
        return !string.matches("\\d+") || (Integer.parseInt(string) <= 0 || Integer.parseInt(string) >= 6);
    }
    static boolean hit(int row, int column, int[][] ship){
        for (int i = 0; i < 3; i++) {
            if ((ship[i][0]==row)&&(ship[i][1]==column)) return true;
        }
        return false;
    }
}
