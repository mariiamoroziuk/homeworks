package homework13.dao;

import homework13.entity.*;
import homework13.logger.FamilyLogger;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class CollectionFamilyDao implements IFamilyDao {

    private List<Family> allFamilies = new ArrayList<>();
    private long idCounter = 1;

    @Override
    public List<Family> getAllFamilies() {
        FamilyLogger.info("получение списка семей");
        return this.allFamilies;
    }

    @Override
    public Family getFamilyByIndex(int index) {
        if(index < 0 || index > this.allFamilies.size() - 1) {
            FamilyLogger.info("получение семьи по индексу: нет семьи с таким индексом");
            return null;
        } else {
            FamilyLogger.info("получение семьи по индексу");
            return this.allFamilies.get(index);
        }
    }

    @Override
    public Family getFamilyById(long id) {
        Family res = null;
        for (Family f : this.allFamilies) {
            if (f.getId() == id) res = f;
        }
        FamilyLogger.info("получение семьи по айди");
        return res;
    }

    @Override
    public boolean deleteFamily(int index) {
        if(index < 0 || index > this.allFamilies.size() - 1) {
            FamilyLogger.info("удаление семьи: нет семьи с таким индексом");
            return false;
        } else {
            FamilyLogger.info("удаление семьи");
            return deleteFamily(this.allFamilies.get(index));
        }
    }

    @Override
    public boolean deleteFamily(Family family) {
        return this.allFamilies.remove(family);
    }

    @Override
    public void saveFamily(Family family) {
        int index = this.allFamilies.indexOf(family);

        if(index > -1){
            FamilyLogger.info("изменение семьи");
            this.allFamilies.set(index, family);
        } else {
            FamilyLogger.info("добавление новой семьи");
            family.setId(idCounter++);
            this.allFamilies.add(family);
        }

    }

    @Override
    public void loadDataToDB(List<Family> families) throws IOException {
        FamilyLogger.info("загрузка семей с БД");
        Database.write(families);
    }

    @Override
    public void loadDataFromDB() throws IOException {
        FamilyLogger.info("сохранение семей в БД");
        this.allFamilies = Database.read();
    }
}
