package homework8;

import java.util.*;

public class Human {
    private String name;
    private String surname;
    private int year;
    private int iq;
    private Map<DayOfWeek, String> schedule = new HashMap<>();
    private Family family;
    public Pet pet;

    static {
        System.out.println("Загружается новый класс Human");
    }
    {
        System.out.println("Создается новый обьект класса Human");
    }

    Human(){
    }
    Human(String name, String surname, int year){
        setName(name);
        setSurname(surname);
        setYear(year);
    }
    Human(String name, String surname, int year, int iq, Map<DayOfWeek, String> schedule){
        this(name, surname, year);
        setIq(iq);
        setSchedule(schedule);
    }

    public String getName(){
        return this.name;
    }
    public void setName(String name) {
        this.name=name;
    }

    public String getSurname(){ return this.surname; }
    public void setSurname(String surname) { this.surname=surname; }

    public int getYear(){ return this.year; }
    public void setYear(int year) { this.year=year; }

    public int getIq(){
        return this.iq;
    }
    public void setIq(int iq) {
        if (iq > 100 || iq < 0) {
            System.out.println("Wrong iq");
            return;
        }
        this.iq=iq;
    }

    public Map<DayOfWeek, String> getSchedule(){
        return this.schedule;
    }
    public void setSchedule(Map<DayOfWeek, String> schedule) {
        this.schedule=schedule;
    }

    public Family getFamily(){ return this.family; }
    public void setFamily(Family family) {
        this.family=family;
    }

    void greetPet(){
        if(this.pet != null )System.out.printf("Привет, %s\n", this.pet.getNickname());
        else System.out.print("So sad that I don't have a pet :(\n");
    }

    void describePet(){
        if(this.pet != null)System.out.printf("У меня есть %s, ему %d лет, он %s\n", this.pet.getSpecies(), this.pet.getAge(), this.pet.getTrickLevel()>50 ? "очень хитрый" : "почти не хитрый");
        else System.out.print("I want a dog!!!\n");
    }

    boolean feedPet(boolean flag){
        if(this.getFamily() != null && this.pet != null){
            if (flag) {
                System.out.printf("Хм... покормлю ка я %s\n", this.pet.getNickname());
                return true;
            } else {
                Random random = new Random();
                int randomNum = random.nextInt(101);
                if (this.pet.getTrickLevel() > randomNum) {
                    System.out.printf("Хм... покормлю ка я %s\n", this.pet.getNickname());
                    return true;
                } else {
                    System.out.printf("Думаю, %s не голоден.\n", this.pet.getNickname());
                    return false;
                }
            }
        }else return false;
    }

    @Override
    public String toString(){
        return this.getClass().getSimpleName()+"{name="+this.getName()+ ", surname="+this.getSurname()+ ", year="+this.getYear()+ ", iq="+this.getIq()+", schedule="+ this.getSchedule()+"}";
    }

    @Override
    public boolean equals(Object obj){
        if (this == obj) return true;
        if (!(obj instanceof Human)) return false;

        Human human = (Human)obj;
        return (Objects.equals(getName(), human.getName()) && Objects.equals(getSurname(), human.getSurname()));
    }

    @Override
    public int hashCode() {
        return Objects.hash(getName(), getSurname());
    }

    @Override
    protected void finalize() throws Throwable {
        System.out.printf("Deleted object: %s", this.toString());
    }
}

