package homework10.entity;

public interface IHumanCreator {
    Human bornChild(String boysName, String girlsName);
}
